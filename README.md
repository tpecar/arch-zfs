# arch-zfs

I hate it when people take a piece of technology that is intended to be simple and go out of their way to overcomplicate it to the point of being unmanagable.
[OpenZFS installation guides](https://openzfs.github.io/openzfs-docs/Getting%20Started/Arch%20Linux/index.html) are a prime example of that.

Now I understand that such complexity has it's purpose and is actually necessary if you intend to use a managed checkpoint system (like zsys and its friends), but if you're a nut like me and would like to manage the filesystem yourself, simplicity is of prime concern.

What follows here is my way of setting up ZFS on ArchLinux.

The guide will get you to a basic bootable system, and you can customize it to your desire after that.

Do note however that I didn't bother making it 'generic' - adjust the command parameters to your system. Do your homework.

## Boot up live CD

Since the stuff below is tested you can get away with a vanilla iso and install the package temporarily with

https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS#Get_ZFS_module_on_archiso_system

If you intend to boot the live CD multiple times, I'd recommend rolling your own iso with zfs-dkms installed

https://wiki.archlinux.org/title/ZFS#Create_an_Archiso_image_with_ZFS_support

Once you've got that, allow yourself to be lazy and start up SSH and connect to it remotely so you can copy & paste the rest.

```sh
# On target machine
passwd                  # set root password
systemctl enable --now sshd.service

# On GUI host, login into the system
ssh root@192.168.0.26   # enter password set at passwd
```
Proceed with installation
```sh
timedatectl set-ntp true
# Assuming you have an installation image with zfs-dkms (custom build)
modprobe zfs
```
## Partition the drive

```sh
gdisk /dev/disk/by-id/ata-Samsung_SSD_860_EVO_mSATA_500GB_S41NNB0K406682R

# ... Set it up to your system ...
# My partition setup as an example
#
# Command (? for help): p
# Disk /dev/disk/by-id/ata-Samsung_SSD_860_EVO_mSATA_500GB_S41NNB0K406682R: 976773168 sectors, 465.8 GiB
# Sector size (logical/physical): 512/512 bytes
# Disk identifier (GUID): 40F770AE-6AD9-467C-84CF-1030AFC71F18
# Partition table holds up to 128 entries
# Main partition table begins at sector 2 and ends at sector 33
# First usable sector is 34, last usable sector is 976773134
# Partitions will be aligned on 2048-sector boundaries
# Total free space is 2014 sectors (1007.0 KiB)
# 
# Number  Start (sector)    End (sector)  Size       Code  Name
#    1            2048         8390655   4.0 GiB     EF00  EFI system partition
#    2         8390656        75499519   32.0 GiB    8200  Linux swap
#    3        75499520       976773134   429.8 GiB   BF00  Solaris root
```
Set up boot, swap partitions
```sh
# We will go with EFI = /boot, since I don't care about system snapshots
mkfs.vfat -F 32 -n EFI /dev/disk/by-id/ata-Samsung_SSD_860_EVO_mSATA_500GB_S41NNB0K406682R-part1 

mkswap /dev/disk/by-id/ata-Samsung_SSD_860_EVO_mSATA_500GB_S41NNB0K406682R-part2 
```
Set up ZFS partition (root pool)
```sh
zpool create \
    -o ashift=12 \
    -o autotrim=on \
    -R /mnt \
    -O acltype=posixacl \
    -O canmount=off \
    -O compression=zstd \
    -O dnodesize=auto \
    -O normalization=formD \
    -O relatime=on \
    -O xattr=sa \
    -O mountpoint=none \
    rpool \
    /dev/disk/by-id/ata-Samsung_SSD_860_EVO_mSATA_500GB_S41NNB0K406682R-part3

zpool list
# NAME    SIZE  ALLOC   FREE  CKPOINT  EXPANDSZ   FRAG    CAP  DEDUP    HEALTH  ALTROOT
# rpool   428G   420K   428G        -         -     0%     0%  1.00x    ONLINE  /mnt
```
Create datasets
```sh
zfs create -o canmount=off rpool/ROOT
zfs create -o canmount=noauto -o mountpoint=/ rpool/ROOT/default

zfs mount rpool/ROOT/default

for i in {root,srv};
do
    zfs create -o canmount=on rpool/ROOT/default/$i
done

chmod 750 /mnt/root
```
We will create per user datasets for their homes
```sh
zfs create -o canmount=off rpool/ROOT/default/home
for i in {tpecar,misc};
do
    # doing useradd here is too early
    # I did it after the fact, that's why it worked
    useradd $i
    zfs create -o canmount=on rpool/ROOT/default/home/$i
    chown $i:$i /home/$i
    chmod go-rx /home/$i
done
```
Final dataset structure
```sh
zfs list
# NAME                             USED  AVAIL     REFER  MOUNTPOINT
# rpool                           1.34M   415G       96K  none
# rpool/ROOT                       688K   415G       96K  none
# rpool/ROOT/default               592K   415G      112K  /mnt
# rpool/ROOT/default/home          288K   415G       96K  /mnt/home
# rpool/ROOT/default/home/misc      96K   415G       96K  /mnt/home/misc
# rpool/ROOT/default/home/tpecar    96K   415G       96K  /mnt/home/tpecar
# rpool/ROOT/default/root           96K   415G       96K  /mnt/root
# rpool/ROOT/default/srv            96K   415G       96K  /mnt/srv
```
## Installation
Mount everything (ZFS partitions are already mounted)
```sh
mkdir /mnt/boot
mount /dev/disk/by-label/EFI /mnt/boot
swapon /dev/disk/by-id/ata-Samsung_SSD_860_EVO_mSATA_500GB_S41NNB0K406682R-part2

# Set up fstab
genfstab -U /mnt >> /mnt/etc/fstab
vim /mnt/etc/fstab      # Remove all ZFS mounts (managed by zfs deamon)
```
Install base + build packages
```sh
# consider adding
#     zsh grml-zsh-config
pacstrap /mnt base base-devel linux linux-firmware linux-headers vim
```
chroot
```sh
arch-chroot /mnt
```
Configure locale, keymap, timezone, hostname to your liking
```sh
echo en_US.UTF-8 UTF-8 >> /etc/locale.gen
locale-gen
systemd-firstboot --force --locale="en_US.UTF-8" --locale-messages="en_US.UTF-8" --keymap=uk --timezone="Europe/Ljubljana" --hostname=tp
```
Set up `zfs-dkms`

Fun fact: 4GB of system memory is enough to build from tmpfs
```sh
cd /tmp
curl https://aur.archlinux.org/cgit/aur.git/snapshot/zfs-utils.tar.gz | tar xzv
chown -R nobody:nobody ./zfs-utils
cd ./zfs-utils/
sudo -u nobody makepkg --skippgpcheck 
pacman -U *.tar.zst 

cd /tmp
curl https://aur.archlinux.org/cgit/aur.git/snapshot/zfs-dkms.tar.gz | tar xzv
chown -R nobody:nobody ./zfs-dkms
cd ./zfs-dkms/
sudo -u nobody makepkg --skippgpcheck
pacman -U *.tar.zst 
```
Generate host id
```sh
zgenhostid -f -o /etc/hostid
```
Enable ZFS services

We will use cache importing instead of scanning `(zfs-import-scan.service)`
```sh
systemctl enable zfs-import-cache.service zfs-mount.service zfs-import.target zfs-zed zfs.target
```
Set up zfs cache, this will be used during boot
```sh
zpool set cachefile=/etc/zfs/zpool.cache rpool
```
Configure mkinitcpio
```sh
vim /etc/mkinitcpio.conf    # HOOKS=(base udev autodetect modconf block keyboard zfs filesystems)
mkinitcpio -P
```
Set up bootloader

We have FAT32 EFI boot partition, we can go with `systemd-boot`
```sh
bootctl install

cat > /boot/loader/loader.conf <<EOF
default  arch.conf
EOF

# Fun fact: zfs=bootfs is a required boot option! Need to read into what it actually does, but without it the pool wouldn't get imported.
cat > /boot/loader/entries/arch.conf <<EOF
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=ZFS=rpool/ROOT/default zfs=bootfs rw
EOF
```
Set up boot configuration for ZFS
```sh
zpool set bootfs=rpool/ROOT/default rpool
```
Set root password
```sh
passwd
```

## Do whatever additonal stuff here.

See [appendix](#appendix) for ideas.

## Cleanup

Exit from chroot
```sh
exit
```
Export the pool
```sh
zfs umount -a
umount /mnt/boot
zfs umount rpool/ROOT/default
zpool export rpool
```
Reboot to new system
```sh
reboot
```
## Appendix

### How to manually set up networking

For when you inevitably forget to install dhcpcd and don't want to dick around with systemd-networkd
```sh
ip add 192.168.0.55/16 dev enp0s25      # host address

ip route flush dev enp0s25
ip route add 192.168.0.1 dev enp0s25    # default gateway
ip route add default via 192.168.0.1
ping 8.8.8.8        # should work

systemctl start systemd-resolved.service
resolvectl status   # should print out DNS configuration
ping www.google.com # should work
```

### Set up a functional i3 + KDE Plasma desktop
```sh
# xorg
pacman -S xorg-server xorg-apps

# session manager
pacman -S sddm
systemctl enable sddm.service

# base plasma install
pacman -S plasma-desktop

# ------------------
# consider using
#    pacman -S kde-network-meta kde-system-meta kde-utilities-meta
# instead of individual packages

# essential widgets
pacman -S plasma-nm plasma-pa powerdevil kscreen
# essential apps
pacman -S ark dolphin kate
# ------------------

# i3 window manager
pacman -S i3 feh terminus-font
curl https://gitlab.com/tpecar/arch-zfs/-/raw/master/etc/i3/config --output /etc/i3/config
curl https://gitlab.com/tpecar/arch-zfs/-/raw/master/etc/i3/bg.png --output /etc/i3/bg.png
```

With plasma version >=5.25 the way window manager gets loaded has changed, and one needs to configure it via systemd

```
cat > /etc/systemd/user/plasma-i3.service <<EOF
[Install]
WantedBy=plasma-workspace.target

[Unit]
Description=Plasma Custom Window Manager
Before=plasma-workspace.target

[Service]
ExecStart=/usr/bin/i3
Slice=session.slice
Restart=on-failure
EOF
```

Globally mask the default user service that starts Kwin, and replace it with i3 user service
```
systemctl --global mask plasma-kwin_x11.service
systemctl --global enable plasma-i3.service
```

> Old-style sddm entry for posterity, not applicable anymore
> ```
> cat > /usr/share/xsessions/plasma-i3.desktop <<EOF
> [Desktop Entry]
> Type=XSession
> Exec=env KDEWM=/usr/bin/i3 /usr/bin/startplasma-x11
> DesktopNames=KDE
> Name=Plasma (i3)
> EOF
> ```

#### Plasma splash screen
In order to remove the 30s spinning screen at login, go to
`System Settings` > `Apperance` > `Workspace Theme` > `Splash Screen` and set it to `None`

#### Plasma pager
Spawn multiple applications on multiple pages to make the pager visible, then configure Pager with `Pager` > `General` > `Display: Desktop name`

#### Plasma task bar
Configure task bar with `Icons-only Task Manager Settings` > `Behaviour` > `Show only tasks : From current desktop`

---
### Set up additional disk drives

I have additional 2TB of spinning rust (Seagate FireCuda ST2000LX001) in the laptop. While I wouldn't consider it a good choice today, it made sense some 5 years ago. I have some big software packages I won't lament losing (I'm looking at you Xilinx), so it still comes handy.

The general structure of datasets
- `dpool/DATA` is the root dataset, `dpool/DATA/default` serves as a container for currently active (promoted) dataset
- any writeable clones would be under `dpool/DATA/{clone-name}`
- `dpool/DATA/*/data` is the system dataset (mounted at `/data`), intended for system-wide external software installations. Accessible for all users.
- `dpool/DATA/*/home/{user}` is the user dataset (mounted at `/home/{user}/data`). Accessible for user only

```sh
zpool create \
    -o ashift=12 \
    -O acltype=posixacl \
    -O canmount=off \
    -O compression=zstd \
    -O dnodesize=auto \
    -O normalization=formD \
    -O relatime=on \
    -O xattr=sa \
    -O mountpoint=none \
    dpool \
    /dev/disk/by-id/ata-ST2000LX001-1RG174_WDZFBQ4D

zfs create -o canmount=off dpool/DATA
zfs create -o canmount=off dpool/DATA/default
```

Create system-wide dataset

```sh
mkdir /data
zfs create -o canmount=on -o mountpoint=/data dpool/DATA/default/data
```

Create per-user datasets

```sh
zfs create -o canmount=off dpool/DATA/default/home
for i in {tpecar,misc};
do
    mkdir /home/$i/data
    zfs create -o canmount=on -o mountpoint=/home/$i/data dpool/DATA/default/home/$i
    chown $i:$i /home/$i/data
done
```

## References

While I'm displeased with their overly complicated presentation, you'll hardly find anything better than OpenZFS docs
- https://openzfs.github.io/openzfs-docs/Getting%20Started/Arch%20Linux/index.html
- https://openzfs.github.io/openzfs-docs/man/8/index.html

most of the installation tricks were picked up from there. Note that my way of setting up ZFS is a far cry from their way of doing things, but that is due to a conscious decision of keeping things simple.

Some good ideas were picked from Arch Wiki

https://wiki.archlinux.org/title/Install_Arch_Linux_on_ZFS

but I wouldn't consider it complete.

In the end, part of the choices taken by the guide are the result of my own experimentation, usage and cursing, done to yield results that I _subjectively_ find good.

Your opinion may (and most probably will) defer, but I at least hope that you've found it interesting, possibly even helpful.

